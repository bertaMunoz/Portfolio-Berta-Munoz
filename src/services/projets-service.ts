import {ajax} from 'jquery';
import { LoginService } from './login-service';
import { Projects } from '../entity/projects';

export class ProjetsService {
    private apiUrl = 'https://www.simplonlyon.fr/promo6/bmunoz/api/rest/projects/'; 

    private loginService = new LoginService();

    findAll():Promise<Projects[]> {
        return ajax(this.apiUrl);
    }

    add(projects:any):Promise<Projects> {
        return ajax(this.apiUrl, {
            headers: {
                'Authorization': 'Bearer '+this.loginService.getToken()
            },
            data: projects,
            method: 'POST',
            processData: false,
            contentType: false 
        }).catch(err => {
            if(err.status === 401) {
                this.loginService.logout();
            }
            throw err;
        });
    }

    remove(id:any):Promise<Projects> {
        return ajax(this.apiUrl+'delete/'+id, {
           
            method: 'DELETE'
    });
   
    }

}
