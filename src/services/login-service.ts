import { ajax } from "jquery";

  export class LoginService {

    private apiUrl = 'https://www.simplonlyon.fr/promo6/bmunoz/api/api/login_check';
    
    getToken():string {
        return sessionStorage.getItem('token');
    }

    login(username:string,password:string) {
        return ajax(this.apiUrl, {
            contentType: 'application/json',
            data: JSON.stringify({username:username, password:password}),
            method:'POST'
        })
        .then(resp => sessionStorage.setItem('token', resp.token));
    }

    logout() {
        sessionStorage.removeItem('token');
    }
}  