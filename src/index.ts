import * as $ from 'jquery';
import { LoginService } from './services/login-service';
import { ProjetsService } from './services/projets-service';


let loginService = new LoginService();
let service = new ProjetsService();
showLogin();
showProjects(service);

function showProjects(service) {
  $('#projectsAll').empty();

  service.findAll().then(data => {

    for (const projects of data) {

      let article = $('<article></article>');
      let a = $('<a></a>');
      let img = $('<img>');
      let p = $('<p></p>');
      let btn = $('<button>Supprimer</button>');
      let h5 = $('<h5></h5>');
      let div = $('<div></div>');
      let ddiv = $('<div></div>');

      if (!loginService.getToken()) {
        $(btn).hide();
      } else {
        $(btn).show();
      }

      div.addClass('card bg-transparent');
      ddiv.addClass('card-body');
      h5.addClass('card-title');
      p.text(projects.description);
      a.attr('href', projects.link).text(projects.name);
      a.addClass('btn');

      img.attr('src', projects.image);
      img.attr('href', projects.ling);
      img.addClass('card-img-top zoom');
      btn.addClass('btn suppr');
      btn.attr('id', projects.id );
      
      


      ddiv.append(h5, p, a)
      div.append(ddiv, img, btn);
      article.append(div);

      $('#projectsAll').append(article);


      btn.click(() => {
        console.log("suppr")
        let errors = $('#errors').empty();
        service.remove(projects.id)
          .then(projects => showProjects(service))
          .catch(err => errors.text('There has been a problem'))
      });
    }
  });
}

$('#toggle-add').on('click', () => $('#add-popup').toggle())

$('#add-form').on('submit', function (event) {
  event.preventDefault();

  let errors = $('#errors').empty();// met à 0 la liste des erreurs
  let data = new FormData(<HTMLFormElement>this); // il ne sait pas que c'est un htmlform element, il attend un html element, donc on lui précise (on caste une variable dans un autre type que ce qu'on a actuellement => bcp utilisé dans les lgges à typage statique. )
  service.add(data)
    .then(projects => showProjects(service))
    .catch(err => errors.text('there has been a problem'))
});
///////////

$('#logout').on('click', () => {
  loginService.logout();
  showLogin(); 
})


$('#login-form').on('submit', function (event) {
  event.preventDefault();
  let errors = $('#errors').empty();
  let username = <string>$('#username').val();
  let password = <string>$('#password').val();
  loginService.login(username, password)
    .then(() => showLogin())
    .catch(err => errors.text('Authentication error'));

});


function showLogin() {
  if (!loginService.getToken()) {

    $('#login-form').show();
    $('#add-form').hide();
    $('#logout').hide();
    $('.suppr').hide();
    
  } else {
    
    $('#login-form').hide();
    $('#add-form').show();
    $('#logout').show();
    $('.suppr').show();

  }
}

